/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : 127.0.0.1:3306
 Source Schema         : air_ticket

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 15/03/2021 17:50:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for airtype
-- ----------------------------
DROP TABLE IF EXISTS `airtype`;
CREATE TABLE `airtype`  (
  `air_autoid` int(11) NOT NULL AUTO_INCREMENT,
  `air_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `air_F` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `air_Fname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `air_Fnumber` smallint(6) NULL DEFAULT NULL,
  `air_C` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `air_Cname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `air_Cnumber` smallint(6) NULL DEFAULT NULL,
  `air_Y` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `air_Yname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `air_Ynumber` smallint(6) NULL DEFAULT NULL,
  `air_totalnumber` smallint(6) NULL DEFAULT NULL,
  `flag` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`air_autoid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of airtype
-- ----------------------------
INSERT INTO `airtype` VALUES (1, '空客A320', NULL, '存在', 30, NULL, '存在', 30, NULL, '存在', 30, 90, 0);
INSERT INTO `airtype` VALUES (2, '波音747', NULL, '存在', 10, NULL, '存在', 20, NULL, '存在', 30, 60, 0);

-- ----------------------------
-- Table structure for bookinformation
-- ----------------------------
DROP TABLE IF EXISTS `bookinformation`;
CREATE TABLE `bookinformation`  (
  `boo_autoid` bigint(20) NOT NULL AUTO_INCREMENT,
  `com_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cus_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `boo_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `boo_aaddress` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `boo_baddress` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `boo_btime` datetime(0) NULL DEFAULT NULL,
  `boo_atime` datetime(0) NULL DEFAULT NULL,
  `boo_berth` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `boo_number` smallint(6) NULL DEFAULT NULL,
  `boo_fare` double NULL DEFAULT NULL,
  `boo_time` datetime(0) NULL DEFAULT NULL,
  `flag_pay` int(11) NULL DEFAULT 1,
  `flag` int(11) NULL DEFAULT 1,
  `boo_ordernum` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cus_account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cus_telnumber` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`boo_autoid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bookinformation
-- ----------------------------
INSERT INTO `bookinformation` VALUES (1, '国强航空', '513436200003158338', 'STC89C52', '上海', '北京', NULL, '2021-03-16 09:20:00', '经济舱', 0, 500, '2021-03-15 08:19:24', 1, 0, '20212151161856', '张小三', '15522222222');
INSERT INTO `bookinformation` VALUES (2, '国强航空', '513436200003156092', 'STC89C52', '上海', '北京', NULL, '2021-03-16 09:20:00', '经济舱', 17, 500, '2021-03-15 08:28:14', 1, 0, '2021215116287', '李四', '18833336666');

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer`  (
  `cus_autoid` int(11) NOT NULL AUTO_INCREMENT,
  `cus_account` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cus_pwd` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cus_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cus_sex` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cus_telnumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cus_email` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cus_integral` int(11) NULL DEFAULT NULL,
  `far_id` smallint(6) NULL DEFAULT NULL,
  `flag` int(11) NULL DEFAULT 1,
  `cus_names` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cus_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cus_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cus_time` date NULL DEFAULT NULL,
  PRIMARY KEY (`cus_autoid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer
-- ----------------------------
INSERT INTO `customer` VALUES (1, '10010', '123465', '130411199901010023', 'm', '15522115672', '6666666@qq.com', 0, NULL, 0, '张三', '张三', '金牌客户', '2021-01-29');

-- ----------------------------
-- Table structure for flightinformation
-- ----------------------------
DROP TABLE IF EXISTS `flightinformation`;
CREATE TABLE `flightinformation`  (
  `fli_autoid` int(11) NOT NULL AUTO_INCREMENT,
  `com_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `air_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fli_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fli_discount` double NULL DEFAULT NULL,
  `fli_baddress` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fli_aaddress` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fli_btime` datetime(0) NULL DEFAULT NULL,
  `fli_atime` datetime(0) NULL DEFAULT NULL,
  `fli_Fnumber` smallint(6) NULL DEFAULT NULL,
  `fli_Cnumber` smallint(6) NULL DEFAULT NULL,
  `fli_Ynumber` smallint(6) NULL DEFAULT NULL,
  `fli_FFare` double NULL DEFAULT NULL,
  `fli_refundtime` datetime(0) NULL DEFAULT NULL,
  `flag` int(11) NULL DEFAULT 1,
  `fli_CFare` double NULL DEFAULT NULL,
  `fli_YFare` double NULL DEFAULT NULL,
  PRIMARY KEY (`fli_autoid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flightinformation
-- ----------------------------
INSERT INTO `flightinformation` VALUES (1, '国强航空', '空客A320', '3u8633', 0, '天津', '成都', '2021-01-28 00:00:00', '2021-01-29 00:00:00', 13, 6, 8, 300, NULL, 0, 200, 100);
INSERT INTO `flightinformation` VALUES (2, '国强航空', '波音747', 'STC89C52', 0, '北京', '上海', '2021-03-16 07:13:00', '2021-03-16 09:20:00', 10, 20, 30, 1000, '1970-01-01 00:00:00', 0, 800, 500);

-- ----------------------------
-- Table structure for manager
-- ----------------------------
DROP TABLE IF EXISTS `manager`;
CREATE TABLE `manager`  (
  `man_autoid` int(11) NOT NULL AUTO_INCREMENT,
  `man_account` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账户',
  `man_pwd` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码吗',
  `man_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证',
  `man_sex` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `man_telnumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话号码',
  `man_email` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电子邮箱',
  `man_time` datetime(0) NULL DEFAULT NULL COMMENT '注册日期',
  `flag` int(11) NULL DEFAULT 0 COMMENT '是否管理员（0：管理员  1：删除）',
  PRIMARY KEY (`man_autoid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of manager
-- ----------------------------
INSERT INTO `manager` VALUES (1, '10086', '1234', '001', '男', '15678995432', '123456@qq.com', '2021-01-27 18:36:36', 0);
INSERT INTO `manager` VALUES (2, 'zhangsan', 'zhangsan', '513436200003158071', 'm', '18844448888', '127001@163.com', '2021-03-15 07:56:07', 0);

SET FOREIGN_KEY_CHECKS = 1;
