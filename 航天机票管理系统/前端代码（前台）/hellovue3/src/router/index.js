import Vue from 'vue'
import VueRouter from 'vue-router'
//import立即加载，用此方法加载的vue不需要向服务器发送请求，立即加载。

Vue.use(VueRouter)

const routes = [
    {


        path: '/',
        name: 'Index',
        component: () => import(/* webpackChunkName: "hello" */ '../views/Index.vue'),
        redirect:"/first",

        //懒加载，进入链接才会想服务器请求加载。
        //箭头函数 function(){}或（）=>{} 左边入参，右边方法体，如果方法只有一行，{}可省略
        children: [
            {
                path: '/order',
                name: 'Order',
                component: () => import('../views/children/order.vue')
            },
            {
                path: '/vip',
                name: 'Vip',
                component: () => import('../views/children/vip.vue')
            },
            {
                path: '/about',
                name: 'About',
                component: () => import('../views/children/about.vue')
            },

            {
                path: '/first',
                name: 'First',
                component: () => import('../views/children/first.vue')
            },

            {
                path: '/chat',
                name: 'Chat',
                component: () => import('../views/children/chat.vue')
            }


        ]
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
