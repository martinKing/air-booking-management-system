import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import element from 'element-ui'
import "element-ui/lib/theme-chalk/index.css"
import axios from 'axios'
import "animate.css/animate.min.css"
import _ from "lodash";


window.axios = axios;
window._ = _;

// axios.interceptors.response.use(response => response, (error) => {
//     //如果是403，就提示重新登录，并提示跳转
//
//     console.log(error)
//     if (error.response.status == 403) {
//         // 需要重新登录
//         app.$message({
//             type: "error",
//             message: "权限不足，请重新登录",
//             onClose: function () {
//                 router.push({path: "/"});
//             }
//         })
//     }
//
// })

Vue.use(element)

Vue.config.productionTip = false

const app = new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
