package edu.ncc.airticket.service;

import edu.ncc.airticket.dao.BookInformationDao;
import edu.ncc.airticket.model.BookInformation;
import org.springframework.stereotype.Service;

@Service
public class BookInformationService extends BaseService<BookInformation, BookInformationDao> {
    public void deleteTicketValid(Integer id){ dao.deleteTicketValid(id);}


}
