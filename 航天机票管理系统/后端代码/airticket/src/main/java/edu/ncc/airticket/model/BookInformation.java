package edu.ncc.airticket.model;

import lombok.Data;

import java.util.Date;

@Data
public class BookInformation extends BaseModel{
    private String cusAccount;
    private String comCode;
    private String cusId;
    private Integer cusAge;
    private String booNo;
    private String boobAddress;
    private String booAAddress;
    private Date booBTime;
    private Date booAtime;
    private String booBerth;
    private String booNumber;
    private String booFare;
    private Date booTime;
    private String flagPay;
    private String booOrderNum;
    private String cusName;
    private String cusTelNumber;
    private String cusPwd;
    private String cusSex;
    private String cusEmail;
    private String cusIntegral;
    private Date cusTime;
    private String cusType;
    private String farId;

}

