package edu.ncc.airticket.dao;

import edu.ncc.airticket.model.Flight;

public interface FlightDao extends BaseDao<Flight> {

}
