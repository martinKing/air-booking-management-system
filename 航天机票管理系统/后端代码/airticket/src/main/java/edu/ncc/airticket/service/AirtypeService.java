package edu.ncc.airticket.service;


        import edu.ncc.airticket.dao.AirtypeDao;
        import edu.ncc.airticket.model.Airtype;
        import org.springframework.stereotype.Service;

@Service
public class AirtypeService extends BaseService<Airtype, AirtypeDao> {

    public  boolean valid(String airCode){
        Airtype condition = new Airtype();
        condition.setAirCode(airCode);
        Airtype air = dao.find(condition);
        return  air != null;
    }

}
