package edu.ncc.airticket.model;

import lombok.Data;

import java.util.Date;

@Data
public class Manager extends BaseModel{
    private String manAccount;
    private String manPwd;
    private String manId;
    private String manSex;
    private String manTelNumber;
    private String manEmail;
    private Date manTime;
}
