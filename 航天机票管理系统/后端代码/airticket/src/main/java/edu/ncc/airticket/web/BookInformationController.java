package edu.ncc.airticket.web;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import edu.ncc.airticket.model.BookInformation;
import edu.ncc.airticket.service.BookInformationService;
import edu.ncc.airticket.sys.DTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.Date;
import java.util.List;


@RestController
public class BookInformationController {
    @Autowired
    private BookInformationService service;
    @ResponseBody
    //@RequestBody代表将前台提交的特定格式的数据转换为java对象




    /**
     * 查询多条BookInformation数据
     * @param
     * @return
     */


    @DeleteMapping("/bookInformation/delete/{id}")
    public String delete(@PathVariable("id") Integer id){
        service.delete(id);
        return "success";
    }

    @DeleteMapping("/bookInformation/charge/{id}")
    public String deleteTicketValid(@PathVariable("id") Integer id) {
        service.deleteTicketValid(id);
        return "success";
    }



    /**
     * 查询分页的BookInformation数据
     * @return
     */
    @PostMapping("/bookInformation/page")
    public PageInfo<BookInformation> page(@RequestBody DTO<BookInformation> dto){
        return service.page(dto.getCondition(),dto.getPageNum(),dto.getPageSize());
    }

    @PostMapping("/bookInformation/pageTicketValid")
    public PageInfo<BookInformation> pageTicketValid(@RequestBody DTO<BookInformation> dto){
        return service.pageTicketValid(dto.getCondition(),dto.getPageNum(),dto.getPageSize());
    }
    @PostMapping("/bookInformation/Ticketpage")
    public PageInfo<BookInformation> pageTicket(@RequestBody DTO<BookInformation> dto){
        return service.pageTicket(dto.getCondition(),dto.getPageNum(),dto.getPageSize());
    }




    @PostMapping("/bookInformation/save")
    public String save (@RequestBody BookInformation bookInformation){

        bookInformation.setBooTime(new Date());
        bookInformation.setFlag(BookInformation.NORMAL);
        service.save(bookInformation);
        return "success";
    }

}
