package edu.ncc.airticket.web;

import com.github.pagehelper.PageInfo;
import edu.ncc.airticket.model.Flight;
import edu.ncc.airticket.model.Manager;
import edu.ncc.airticket.service.FlightService;
import edu.ncc.airticket.service.ManagerService;
import edu.ncc.airticket.sys.DTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@RestController与@Controller的区别：RestController不经过视图计息期，而是直接以某种格式返回数据，这里我们返回json格式的数据
@RestController
public class FlightController {

    @Autowired
    private FlightService service;

//
    /**
     * 新增或者修改flight表的数据
     *
     * @param flight
     * @return
     */
    @PostMapping("/flight/save")
    public String save(@RequestBody Flight flight) {
//        flight.setManTime(new Date());
        flight.setFlag(Flight.NORMAL);
        service.save(flight);
        return "success";
    }


    /**
     * 查询多条flight数据
     *
     * @return
     */
    @PostMapping("/flight/findListAddFlight")
    public List<Flight> findListAddFlight() {
        return service.findListAddFlight();
    }




    @PostMapping("/flightinformation/page")
    public PageInfo<Flight> page(@RequestBody DTO<Flight> dto){
        return service.page(dto.getCondition(),dto.getPageNum(),dto.getPageSize());
    }

    /**
     * 通过id删除manager数据
     * @param id
     * @return
     */
    @DeleteMapping("/flightinformation/delete/{id}")
    public String delete(@PathVariable("id") Integer id) {
        service.delete(id);
        return "success";
    }



}
