package edu.ncc.airticket.service;

import edu.ncc.airticket.dao.CustomerDao;
import edu.ncc.airticket.model.Customer;
import org.springframework.stereotype.Service;

@Service
public class CustomerService extends BaseService<Customer, CustomerDao> {

}
