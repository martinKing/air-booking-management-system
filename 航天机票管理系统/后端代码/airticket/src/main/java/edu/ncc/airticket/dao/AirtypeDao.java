package edu.ncc.airticket.dao;

import edu.ncc.airticket.model.Airtype;


public interface AirtypeDao extends BaseDao<Airtype> {

}
