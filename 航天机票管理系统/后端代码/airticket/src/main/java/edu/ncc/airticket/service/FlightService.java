package edu.ncc.airticket.service;

import edu.ncc.airticket.dao.FlightDao;
import edu.ncc.airticket.model.Flight;
import org.springframework.stereotype.Service;

@Service//Service注解用于表明这是个service，同时和component注解作用一致，加入到IOC容器
public class FlightService extends BaseService<Flight, FlightDao> {



}
