package edu.ncc.airticket.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import edu.ncc.airticket.dao.BaseDao;
import edu.ncc.airticket.model.BaseModel;
import edu.ncc.airticket.model.Flight;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class BaseService<T extends BaseModel, D extends BaseDao<T>> {
    /*
     *spring4 的泛型依赖注入，可以让我们只在父类当中进行注入
     */
    @Autowired
    protected D dao;


    public void save(T t) {
        if (t.getId() == null || "".equals(t.getId())) {
            dao.insert(t);
        } else {
            dao.update(t);
        }

    }

    public void delete(T t) {
        dao.delete(t);
    }

    public void delete(Integer id) {
        dao.delete(id);
    }



    public T find(T condition) {
        return dao.find(condition);
    }

    public T findById(Integer id) {
        return dao.findById(id);
    }

    public List<T> findList(T condition) {
        return dao.findList(condition);
    }

    //syz
    public List<T> findListAddFlight() {
        return dao.findListAddFlight();
    }

    public List<T> findListTicketValid(T condition) {
        return dao.findListTicketValid(condition);
    }

    public List<T> findAll() {
        return dao.findAll();
    }



    /**
     * 分页查询数据
     *
     * @param condition  查询条件
     * @param pageNumber 查第几页
     * @param pageSize   每页有多少条
     * @return
     */
    public PageInfo<T> page(T condition, Integer pageNumber, Integer pageSize) {
        return PageHelper.startPage(pageNumber, pageSize).doSelectPageInfo(() -> dao.findList(condition));
    }
    public PageInfo<T> pageTicketValid(T condition, Integer pageNumber, Integer pageSize) {
        return PageHelper.startPage(pageNumber, pageSize).doSelectPageInfo(() -> dao.findListTicketValid(condition));
    }
    public PageInfo<T> pageTicket(T condition, Integer pageNumber, Integer pageSize) {
        return PageHelper.startPage(pageNumber, pageSize).doSelectPageInfo(() -> dao.findListTicket(condition));
    }

}
