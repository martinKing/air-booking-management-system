package edu.ncc.airticket.service;

import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import edu.ncc.airticket.dao.MailServer;
import edu.ncc.airticket.model.Email;
import org.apache.logging.log4j.message.SimpleMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;



/**
 * @author wangjie:
 * @version 创建时间：2019年8月27日 上午10:13:58
 * @Description 类描述:
 */
@Service
public class MailServerImpl implements MailServer {

    @Value("${spring.mail.username}")
    private String fromUser;

    @Autowired
    private JavaMailSender javaMailSender;

    public String getFromUser() {
        return fromUser;
    }


    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }


    @Override
    public void sendMailServer(String sendUser, String title, String text) {
        //创建邮件的实体 用于封装发送邮件需要的信息
        SimpleMailMessage simpleMailMessage=new  SimpleMailMessage();
        //邮件的发送人
        simpleMailMessage.setFrom(fromUser);
        //邮件接收人
        simpleMailMessage.setTo(sendUser);
        //邮件的标题
        simpleMailMessage.setSubject(title);
        //邮件的内容
        simpleMailMessage.setText(text);
        //发送邮件
        javaMailSender.send(simpleMailMessage);
    }



    protected MailServer mailServer;
    @Override
    public void insert(Email email) {
        if (email.getId()== null || "".equals(email.getId())) {
            mailServer.insert(email);
        }
    }

//    @Override
//    public void find() {
//
//    }


//    @Override
//    public void sendFileMail(String sendUser, String title, String text, File file) {
//
//        MimeMessage mimeMessage = null;
//
//        try {
//            mimeMessage =javaMailSender.createMimeMessage();
//            //创建mimeMessageHelper对象用于处理带有附件的邮件信息
//            MimeMessageHelper mimeMessageHelper=new MimeMessageHelper(mimeMessage,true);
//            mimeMessageHelper.setFrom(fromUser);
//            mimeMessageHelper.setTo(sendUser);
//            mimeMessageHelper.setSubject(title);
//            mimeMessageHelper.setText(text);
//            FileSystemResource r = new FileSystemResource(file);
//            //添加附件
//            mimeMessageHelper.addAttachment("附件", r);
//            javaMailSender.send(mimeMessage);
//        } catch (MessagingException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//    }

}
