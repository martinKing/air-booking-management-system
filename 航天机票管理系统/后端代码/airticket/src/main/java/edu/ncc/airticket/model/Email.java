package edu.ncc.airticket.model;

import lombok.Data;

@Data
public class Email {
    private String  name;
    private  String text;
    private  String email;
    private Integer id;
}
