package edu.ncc.airticket.dao;

import edu.ncc.airticket.model.BaseModel;

import java.util.List;

public interface BaseDao<T extends BaseModel> {
    void insert(T t);

    void update(T t);

    void delete(T t);

    void delete(Integer id);

    //查询单条
    T find(T condition);
    //id查
    T findById(Integer id);
    //查多条
    List<T>  findList(T condition);
    List<T>  findListTicketValid(T condition);
    //查所有
    List<T> findAll();
    //syz
    List<T>  findListAddFlight();

    List<T>  findListTicket(T condition);

    void deleteTicket(Integer id);






}
