package edu.ncc.airticket.model;

import lombok.Data;

@Data
public class BaseModel {
    protected Integer id;
    protected String flag;

    public static  final String NORMAL="0";
    public static  final String dRLETE="1";
    public static  final String REFUND="2";
    public static  final String SUCCESS="3";



}
