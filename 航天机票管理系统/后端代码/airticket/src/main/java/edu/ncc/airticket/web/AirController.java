package edu.ncc.airticket.web;

import com.github.pagehelper.PageInfo;
import edu.ncc.airticket.model.Airtype;
import edu.ncc.airticket.sys.DTO;

import edu.ncc.airticket.model.Manager;
import edu.ncc.airticket.service.AirtypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


//@RestController与@Controller的区别:RestController不经过视图解析器，
//直接以某种格式返回数据，我们用json
@RestController
public class AirController {
    @Autowired
    private AirtypeService service;


    @PostMapping("/air/save")
    public String save(@RequestBody Airtype air) {
        air.setFlag(Airtype.NORMAL);
        service.save(air);
        return "success";
    }

    /**
     * 通过id删除manager数据
     *
     * @param id
     * @return
     */
    @DeleteMapping("/air/delete/{id}")
    public String delete(@PathVariable("id") Integer id) {
        service.delete(id);
        return "success";
    }


    @GetMapping("/air/valid/{airCode}")
    public Map<String, Object> valid(@PathVariable("airCode") String manAccount) {
        boolean dup = service.valid(manAccount);
        Map<String, Object> rs = new HashMap<>();
        rs.put("dup", dup);
        return rs;
    }


    /**
     * 查询分页的manager数据
     *
     * @return
     */
    @PostMapping("/air/page")
    public PageInfo<Airtype> page(@RequestBody DTO<Airtype> dto) {
        return service.page(dto.getCondition(), dto.getPageNum(), dto.getPageSize());
    }



    @PostMapping("/air/findList")
    public List<Airtype> findList(@RequestBody Airtype condition){
        return service.findList(condition);
    }


}






