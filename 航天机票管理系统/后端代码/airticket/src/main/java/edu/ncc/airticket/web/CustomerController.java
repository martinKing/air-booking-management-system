package edu.ncc.airticket.web;

import com.github.pagehelper.PageInfo;
import edu.ncc.airticket.model.Customer;
import edu.ncc.airticket.model.Manager;
import edu.ncc.airticket.service.CustomerService;
import edu.ncc.airticket.sys.DTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@RestController
public class CustomerController {
    @Autowired
    private CustomerService service;

    @PostMapping("/customer/page")
    public PageInfo<Customer> page(@RequestBody DTO<Customer> dto){
//        return service.page( dto.getCondition(),dto.getPageNum(),dto.getPageSize());
        PageInfo<Customer> page = service.page(dto.getCondition(), dto.getPageNum(), dto.getPageSize());
        List<Customer> list = page.getList();
        System.out.println(list);
        return page;
    }
    @DeleteMapping("/customer/delete/{id}")
    public String delete(@PathVariable Integer id) {
        service.delete(id);
        return "success";
    }

    @RequestMapping("/children/login")
    public Customer managerLogin(@RequestBody Customer customer, HttpSession session, HttpServletResponse resp){
        //开始进行判断，通过访问数据库查询manager对象，如果根据用户名和密码查到了Manager对象
        //登录成功，查不到就返回null，代表登录失败
        Customer principal = service.find(customer);
        //select * from manager where man_account="zhangsan" and man_pwd="123456"
        if (principal!=null){
            //登录成功
            session.setAttribute("principal",principal);
            return principal;
        }
        //如果失败证明密码或者用户名输入错误，在这里除了返回空数据以外，设置相应得状态码为403
        resp.setStatus(403);
        return null;

    }



    @PostMapping("/children/register")
    public String save(@RequestBody Customer customer){
        customer.setCusTime(new Date());
        customer.setFlag(customer.NORMAL);
        customer.setCusIntegral(0);
        customer.setCusType("金牌客户");
        service.save(customer);
        return "success";
    }
}
