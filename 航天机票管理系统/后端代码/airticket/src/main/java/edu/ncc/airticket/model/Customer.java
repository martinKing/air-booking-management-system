package edu.ncc.airticket.model;

import lombok.Data;

import java.util.Date;
@Data
public class Customer extends BaseModel {
    private String cusAccount;
    private String cusPwd;
    private String cusId;
    private String cusSex;
    private String cusTelNumber;
    private String cusEmail;
    private Date cusTime;
    private Integer cusIntegral;
    private Integer farId;
    private String cusName;
    private String cusType;
    private String cusNames;

    //
    private String booNo;
    private Date boobTime;
    private String booOrderNum;
    private String bookFlag;
    private String age;
}
