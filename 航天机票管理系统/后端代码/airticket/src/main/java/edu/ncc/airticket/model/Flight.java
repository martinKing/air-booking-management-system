package edu.ncc.airticket.model;

import lombok.Data;

import java.util.Date;

@Data
public class Flight extends BaseModel{
    private String fliNo;
    private String comCode;
    private String airCode;
    private double fliDiscount;
    private String fliBaddress;
    private String fliAaddress;
    private String fliBtime;
    private String fliAtime;
    private int fliFnumber;
    private int fliCnumber;
    private int fliYnumber;
    private double fliFfare;
    private double fliCfare;
    private double fliYfare;
    private Date fliRefundtime;

}
