import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Login',
        component: () => import('../views/Login.vue')
    },
    {
        path: '/index',
        name: 'Index',
        component: () => import('../views/Index.vue'),
        children:[
            {

                path: 'addUser',
                name: 'AddUser',
                component: () => import('../views/manager/AddUser.vue'),
            },
            {

                path: 'refund',
                name: 'Refund',
                component: () => import('../views/bookInformation/Refund.vue'),
            },
            {

                path: 'userAdmin',
                name: 'UserAdmin',
                component: () => import('../views/manager/ManagerList.vue'),
            },
              {
                  path:'addAir',
                      name:'AddAir',
                  component:()=>import('../views/flightinformation/AddFlight')
              },
            {
                path:'customer',
                name:'Customer',
                component:()=>import('../views/customer/Customer')
            },
            {
                path:'/index/searchAir',
                name:'SearchAir',
                component:()=>import('../views/flightinformation/FlightInformationList')
            },
            {
                path: 'ticketValid',
                name: 'TicketValid',
                component: () => import('../views/bookInformation/TicketValid')
            },
            {
                path:'addType',
                name:'AddAir',
                component:()=>import('../views/airtype/AddAirtype')
            },
            {
                path:'airList',
                name:'AirList',
                component:()=>import('../views/airtype/AirtypeList')
            },
        ]
    },
    {
        path: '/about',
        name: 'About',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    },
    {
        path: "/hello",
        name: 'Hello',
        component: () => import('../views/Hello.vue')
    }

]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
