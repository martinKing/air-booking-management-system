import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    manager:{}
  },
  mutations: {

    setManager(state,payload){
      state.manager=payload;
    }
  },
  getters:{
    getManager(state){
      return state.manager;
    }
  },
  actions: {
  },
  modules: {
  }
})
